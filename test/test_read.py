from py_cfg import Config, key
from py_cfg.source import AnyserFileConfigSource
import pytest


class TestConfig(Config):
    string = key(key_name="string", cast=str)


def test_read():
    cfg = TestConfig(
        sources=[
            AnyserFileConfigSource(
                fileFormat="yaml", filename="test/test_data/test.yaml"
            )
        ]
    )
    cfg.read()
    assert cfg.string == "hello"
