# py-cfg

Python config utility library that is based on [typed-config](https://github.com/bwindsor/typed-config). The main difference is that this library adds a multi-file format config source and a automatic write-on-set method.
